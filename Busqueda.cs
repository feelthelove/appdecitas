﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeeltheLove2
{
	public class Busqueda
	{
		public void BusquedaDePerfiles()
		{
			int numperfil;
			Console.WriteLine("       BUSQUEDA DE PERFILES \n" +
			"Puedes buscar personas según tus intereses \n " +
			"Escribe cuáles son tus intereses: \n " +
			"Escribe tu primer interés: ");
			string interes1 = Console.ReadLine();
			Console.WriteLine("Escribe tu segundo interés:");
			string interes2 = Console.ReadLine();
			Console.WriteLine("Escribe tu tercer interés:");
			string interes3 = Console.ReadLine();
			do
			{
				Console.Clear();
				Console.WriteLine("Muchas Gracias :D! \n " +
				"Hemos enontrado estos perfiles para tí: \n " +
				" 1. Perfil 1 \n " +
				" 2. Perfil 2 \n " +
				" 3. Regresar \n " +
				"Ingresa el número del perfil que quieras visitar");
				numperfil = int.Parse(Console.ReadLine());
				switch (numperfil)
				{
					case 1:
						Console.Clear();
						Console.WriteLine("Este es el Perfil de: Juan \n " +
						"Puedes realizar las siguientes acciones: \n " +
						"   1. Interesado \n " +
						"   2. Me Gusta \n " +
						"   3. Reportar Usuario \n " +
						"Presiona una tecla para regresar");
						Console.ReadKey();
						break;
					case 2:
						Console.Clear();
						Console.WriteLine(" Este es el Perfil de: Carla \n " +
						"Puedes realizar las siguientes acciones: \n " +
						"   1. Interesado \n " +
						"   2. Me Gusta \n " +
						"   3. Reportar Usuario \n " +
						"Presiona una tecla para regresar");
						Console.ReadKey();
						break;
					case 3:
						Console.Clear();
						break;
					default:
						Console.WriteLine("NO hemos encontrado el número de perfil ingresado... \n" +
						"Asegúrate de ingresar uno existente. Presiona una tecla para volver a intentarlo.");
						Console.Clear();
						break;
				}
				Console.ReadKey();
			} while (numperfil != 3);
		}
	}
}
