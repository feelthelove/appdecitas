﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeeltheLove2
{
	class Mensaje_Bienvenida
	{
		public void Bienvenida()
		{
			Console.WriteLine("Bienvenid@. Ahora puedes navegar por la interfaz de la aplicación");
			Console.WriteLine("  1. Búsqueda");
			Console.WriteLine("  2. Perfiles Registrados");
			Console.WriteLine("  3. Tu perfil");
			Console.WriteLine("  4. Más opciones");
			Console.WriteLine("  5. Cerrar Sesión");
		}
	}
}
