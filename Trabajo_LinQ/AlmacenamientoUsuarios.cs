﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Trabajo_LinQ
{
    class AlmacenamientoUsuarios
    {
        //Se crea una lista para almacenar los usarios que se creen
        public List<InformaciónUsuarios> ListaDeDatos;
        public AlmacenamientoUsuarios()
        {
            
            ListaDeDatos = new List<InformaciónUsuarios>();

            ListaDeDatos.Add(new InformaciónUsuarios { CodigoDeUsuario = 1, Nombre = "Jordan", Apellido = "Figueroa", 
                Cedula = 1306473821, Ciudad = "Portoviejo", Sexo = "Masculino", Edad = 22, Correo = "jordanF@gmail.com", Celular = 0987326153});
            ListaDeDatos.Add(new InformaciónUsuarios { CodigoDeUsuario = 2, Nombre = "Karen", Apellido = "Montes", 
                Cedula = 1314443181, Ciudad = "Manta", Sexo = "Femenino", Edad = 21, Correo = "monteskaren2000@gmail.com", Celular = 0983990324});
            ListaDeDatos.Add(new InformaciónUsuarios { CodigoDeUsuario = 3, Nombre = "Daniel", Apellido = "Muentes", 
                Cedula = 1314443181, Ciudad = "Manta", Sexo = "Masculino", Edad = 24, Correo = "muentes02@gmail.com", Celular = 0983749217});
            ListaDeDatos.Add(new InformaciónUsuarios { CodigoDeUsuario = 4, Nombre = "María", Apellido = "Zambrano", 
                Cedula = 1314443181, Ciudad = "Manta", Sexo = "Femenino", Edad = 23, Correo = "mary10@gmail.com", Celular = 0988562149});
            ListaDeDatos.Add(new InformaciónUsuarios { CodigoDeUsuario = 5, Nombre = "Joel", Apellido = "Grain", 
                Cedula = 1314443181, Ciudad = "Portoviejo", Sexo = "Masculino", Edad = 21, Correo = "joelgrain21@gmail.com", Celular = 0988475132});
        }
        //Primer operación de Linq para mostrar todos los usuarios
        public void MostrarTodosLosUsuarios()
        {

            IEnumerable<InformaciónUsuarios> allusuarios = from usuarios in ListaDeDatos
                                                           select usuarios;

            foreach (InformaciónUsuarios información in allusuarios)
            {
                Console.WriteLine(información.Nombre + " " + información.Apellido);
            }
        }
        //Segunda operación de Linq para buscar y mostrar los usuarios según su código
        public void BuscarPorCodigo()
        {

            IEnumerable<InformaciónUsuarios> codigo = from usuarios in ListaDeDatos
                                                      where usuarios.CodigoDeUsuario == 1
                                                      select usuarios;

            foreach (InformaciónUsuarios información in codigo)
            {
                información.DatosDeUsuario();
            }
        }
        //Operación que permite Mostrar los datos del usuario según su lugar de residencia
        public void BuscarPorLugar()
        {

            IEnumerable<InformaciónUsuarios> ciudad = from usuarios in ListaDeDatos
                                                      where usuarios.Ciudad == "Manta"
                                                      select usuarios;
            
            foreach (InformaciónUsuarios información in ciudad)
            {
                Console.WriteLine(información.Nombre + " " + información.Apellido + " --> " + información.Ciudad);
            }
        }
        //Operación que muestra los usuarios con su edad en forma ascendente
        public void OrdenarPorEdad()
        {

            IEnumerable<InformaciónUsuarios> Ascendente = from usuarios in ListaDeDatos
                                                          orderby usuarios.Edad ascending
                                                          select usuarios;

            foreach (InformaciónUsuarios información in Ascendente)
            {
                Console.WriteLine(información.Nombre + " " + información.Apellido + " con: " + información.Edad + " años");
            }
        }
        //Operación que muestra mediante filtrado solo a los usuarios que tienen números par en su edad
        public void UsuariosConEdadPar()
        {
            IEnumerable<InformaciónUsuarios> EdadPar = from usuarios in ListaDeDatos
                                                          where (usuarios.Edad % 2) == 0
                                                          select usuarios;

            foreach (InformaciónUsuarios información in EdadPar)
            {
                Console.WriteLine(información.Nombre + " " + información.Apellido + " con: " + información.Edad + " años");
            }
        }
        //Aquí se muestra la cantidad de hombres y mujeres existentes, proporcionando su información
        public void CantidadDeHombresYMujeres()
        {
            IEnumerable<InformaciónUsuarios> hombresymujeres = from usuarios in ListaDeDatos
                                                      where usuarios.Sexo == "Masculino"
                                                      select usuarios;
            var cantidadhombres = hombresymujeres.Count();
            Console.WriteLine("La Cantidad de hombres registrados es: " + cantidadhombres);


            foreach (InformaciónUsuarios información in hombresymujeres)
            {
                Console.WriteLine(información.Nombre + " " + información.Apellido + " --> " + información.Sexo);
            }

            Console.WriteLine(" ");

            IEnumerable<InformaciónUsuarios> hombresymujeres1 = from usuarios in ListaDeDatos
                                                              where usuarios.Sexo == "Femenino"
                                                              select usuarios;
            var cantidadmujeres = hombresymujeres1.Count();
            Console.WriteLine("La Cantidad de mujeres registradas es: " + cantidadmujeres);

            foreach (InformaciónUsuarios información in hombresymujeres1)
            {
                Console.WriteLine(información.Nombre + " " + información.Apellido + " --> " + información.Sexo);
            }
        }
    }
}
