﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Trabajo_LinQ
{
    class Program 
    {
        static void Main(string[] args)
        {
            //Primer Caso
            Console.WriteLine("Usuarios Registrados: \n");

            AlmacenamientoUsuarios usuarios = new AlmacenamientoUsuarios();
            usuarios.MostrarTodosLosUsuarios();

            Console.ReadKey();
            Console.Clear();

            //Segundo Caso
            Console.WriteLine("Información del Usuario: \n");

            usuarios.BuscarPorCodigo();

            Console.ReadKey();
            Console.Clear();

            //Tercer Caso
            Console.WriteLine("Nombre de los usuarios dependiendo su ciudad: \n");
 
            usuarios.BuscarPorLugar();

            Console.ReadKey();
            Console.Clear();

            //Cuarto Caso
            Console.WriteLine("Usuarios ordenadas por edad de manera ascendente: \n");

            usuarios.OrdenarPorEdad();

            Console.ReadKey();
            Console.Clear();

            //Quinto Caso
            Console.WriteLine("Nombre de los usuarios con edad par: \n");

            usuarios.UsuariosConEdadPar();

            Console.ReadKey();
            Console.Clear();

            //Sexto Caso
            Console.WriteLine("Cantidad de usuarios Hombres y Mujeres registrados \n");

            usuarios.CantidadDeHombresYMujeres();

            Console.ReadKey();
        }
    }
}