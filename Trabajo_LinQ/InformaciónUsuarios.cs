﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trabajo_LinQ
{
    public class InformaciónUsuarios
    {
        //Se crean las varibales que contendrán cada uno de los datos que se quiera ingresar
        public int CodigoDeUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Cedula { get; set; }
        public string Ciudad { get; set; }
        public string Sexo { get; set; }
        public int Edad { get; set; }
        public string Correo { get; set; }
        public int Celular { get; set; }
        public void DatosDeUsuario()
        {
            //Se imprime por pantalla los datos que se desea mostrar
            Console.WriteLine("Nombre: {0} \n" +
                "Apellido: {1} \n" +
                "Código de Usuario: {2} \n" +
                "Cédula: {3} \n" +
                "Dirección: {4} \n" +
                "Sexo: {5} \n" +
                "Edad: {6} \n" +
                "Correo electrónico: {7} \n" +
                "Número Celular: {8}\n"
                , Nombre, Apellido, CodigoDeUsuario, Cedula, Ciudad, Sexo, Edad, Correo, Celular);
        }
    }
}
