﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeeltheLove2
{
	class MasOpciones
	{
		int numero_de_opcion;
		int boton1;
		int boton2;
		public void Mas_opciones()
		{
			do
			{
				Console.Clear();
				Console.WriteLine("Aquí encontraras más opciones: \n" +
				"   1. Notificaciones \n" +
				"   2. Ideas y Consejos \n" +
				"   3. Botón de Pánico \n" +
				"   4. Regresar \n" +
				"Ingresa el número de la opción que deseas ejecutar: ");
				numero_de_opcion = int.Parse(Console.ReadLine());
				Console.Clear();
				switch (numero_de_opcion)
				{
					case 1:
						Console.WriteLine("  Estas son tus notificaciones: \n" +
						"    **no tiene notificaciones, nadie lo quiere");
						Console.ReadKey();
						break;
					case 2:
						Console.Clear();
						Console.WriteLine("Aquí encontrarás ideas y Consejos \n" +
						"  Así podrás tener más información para saber como actuar en una cita :D");
						Console.ReadKey();
						break;
					case 3:
						Console.Clear();
						Console.WriteLine("Estás en PELIGRO? \n" +
						" Si es así, presiona 1 y se enviará tu dirección actual a tus contactos de confianza: ");
						boton1 = int.Parse(Console.ReadLine());
						if (boton1 == 1)
						{
							Console.WriteLine("Tu ubicación ha sido enviada con éxito!");
						}
						else
						{
							do
							{
								Console.Clear();
								Console.WriteLine("Ingresa el número 1 !!");
								boton2 = int.Parse(Console.ReadLine());
							} while (boton2 != 1);
							Console.WriteLine("Tu ubicación ha sido enviada con éxito!");
						}
						Console.ReadKey();
						break;
					case 4:
						Console.Clear();
						break;
					default:
						Console.WriteLine(" Ingrese una opción válida!. Presiona una tecla para volver a Intentarlo.");
						break;
				}
				Console.ReadKey();
			} while (numero_de_opcion != 4);
		}
	}
}