﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeeltheLove2
{
    class PerfilesRegistrados
    {
        public void PrimerPerfil()
        {
            Idatos_de_usuario idatos_de_usuario = new Primer_Usuario();
            Usuarios usuarios = new Usuarios(idatos_de_usuario);
            usuarios.DatosdelUsuario();
        }
        public void SegundoPerfil()
        {
            Idatos_de_usuario idatos_de_usuario = new Segundo_Usuario();
            Usuarios usuarios = new Usuarios(idatos_de_usuario);
            usuarios.DatosdelUsuario();
        }
    }
}